package com.informaticapinguela.modelo;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory;
import org.apache.tomcat.jdbc.pool.DataSource;

public class UsuarioDAO {
	ResultSet resultSet = null;

	public static void getUsuarios(){//Para pruebas, eliminar/comentar en produccion
		try{  
			Connection connection = PoolBD.getConexion();


			Statement stmt=connection.createStatement(); 
			ResultSet rs = stmt.executeQuery("select * from usuario");  
			while(rs.next()) { System.out.println(rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getString(3)); }
			connection.close();
		}catch(Exception e){ System.out.println(e);}  
	}
	
	public static boolean usuarioValidoRegistro(Usuario usuario) throws SQLException {
		System.out.println("Validando user");
		Connection connection = PoolBD.getConexion();
		String consultaSql = "SELECT * FROM user WHERE nickName= ? UNION SELECT * FROM user WHERE email=?;";
		PreparedStatement stmt= connection.prepareStatement(consultaSql);
		stmt.setString(1, usuario.getNickName());
		stmt.setString(2, usuario.getEmail());
		ResultSet rs = stmt.executeQuery();
		connection.close();
		if(rs.next()) {
			return false;
		}else {
			return true;
		}
	}
	public static Usuario getUsuario(String nickName) throws Exception{
		Connection connection = PoolBD.getConexion();
		System.out.println("Get usuario");
		String consultaSql = "select * from user where nickName= ?";
		PreparedStatement stmt= connection.prepareStatement(consultaSql);
		stmt.setString(1, nickName);
		ResultSet rs = stmt.executeQuery();
		System.out.println("Cerrada!:"+connection.isClosed());
		rs.next();
		connection.close();
		return new Usuario(rs.getString("name"), rs.getString("surName"), rs.getString("nickName"),rs.getString("email"),rs.getString("password"),rs.getString("role"),rs.getString("image"));
	}
	public static void putUsuario(Usuario newUsuario) throws Exception{
		Connection connection = PoolBD.getConexion();

		String consultaSql = "INSERT INTO user(nickName, name, email, role, image, surName, password) VALUES(?,?,?,?,?,?,?);";
		PreparedStatement stmt= connection.prepareStatement(consultaSql);
		stmt.setString(1, newUsuario.getNickName());
		stmt.setString(2, newUsuario.getName());
		stmt.setString(3, newUsuario.getEmail());
		stmt.setString(4, newUsuario.getRole());;
		stmt.setString(5, newUsuario.getImage());
		stmt.setString(6, newUsuario.getSurName());
		stmt.setString(7, newUsuario.getPassword());
		
		ResultSet rs = stmt.executeQuery();
		connection.close();
	}
	public static void updateUsuario(Usuario usuarioNew) throws Exception{
		Connection connection = PoolBD.getConexion();
		String consultaSql = "UPDATE user SET name=?, email=?, surname=?, image=? WHERE nickname=?";
		PreparedStatement stmt = connection.prepareStatement(consultaSql);
		stmt.setString(1, usuarioNew.getName());
		stmt.setString(2, usuarioNew.getEmail());
		stmt.setString(3, usuarioNew.getSurName());
		stmt.setString(4, usuarioNew.getImage());
		stmt.setString(5, usuarioNew.getNickName());
		
		ResultSet rs = stmt.executeQuery();
		connection.close();
	}
	public static void deleteUsuario(String nickName) throws Exception{
		Connection connection = PoolBD.getConexion();

		String consultaSql = "DELETE from usuario WHERE nickName=?;";
		PreparedStatement stmt= connection.prepareStatement(consultaSql);
		stmt.setString(1, nickName);

		ResultSet rs = stmt.executeQuery();
		connection.close();
	}
	public static Integer getNumberOfFollowers(String nickName) throws Exception{
		Connection connection = PoolBD.getConexion();
		String consultaSql = "SELECT COUNT(follower) FROM follow WHERE user=?";
		PreparedStatement stmt = connection.prepareStatement(consultaSql);
		stmt.setString(1, nickName);
		ResultSet rs = stmt.executeQuery();
		connection.close();
		
		if(rs.next()) {
			return rs.getInt(1);
		}else {
			throw new Exception();
		}
		
	}
	public static Integer getNumberOfFollowing(String nickName) throws Exception{
		Connection connection = PoolBD.getConexion();
		String consultaSql = "SELECT COUNT(user) FROM follow WHERE follower=?";
		PreparedStatement stmt = connection.prepareStatement(consultaSql);
		stmt.setString(1, nickName);
		ResultSet rs = stmt.executeQuery();
		connection.close();
		
		if(rs.next()) {
			return rs.getInt(1);
		}else {
			throw new Exception();
		}
	}
	public static Integer getNumberOfPosts(String nickName) throws Exception{
		Connection connection = PoolBD.getConexion();
		String consultaSql = "SELECT COUNT(nickName) FROM post WHERE nickName=?";
		PreparedStatement stmt = connection.prepareStatement(consultaSql);
		stmt.setString(1, nickName);
		ResultSet rs = stmt.executeQuery();
		connection.close();
		
		if(rs.next()) {
			return rs.getInt(1);
		}else {
			throw new Exception();
		}
	}
}
