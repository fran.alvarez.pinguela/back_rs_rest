package com.informaticapinguela.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


public class PostDAO {

	public static ArrayList<Post> getPost(String nickName) throws Exception{
		ArrayList<Post> listaPostUsuario = new ArrayList<Post>();
		String consultaSql ="select * from post where nickName=?";
		Connection connection = PoolBD.getConexion();
		PreparedStatement stmt=connection.prepareStatement(consultaSql);
		ResultSet resultSet = null;
		
		stmt.setString(1, nickName);
		ResultSet rs = stmt.executeQuery(); 

		while(rs.next()) { 
			Post post = new Post(rs.getLong("id"),rs.getString("nickName"),rs.getLong("idReferencia"),rs.getString("texto"),rs.getString("urlFile"));
			listaPostUsuario.add(post);
		}

		connection.close();
		return listaPostUsuario;

	}
	public static void putPost(Post post) throws Exception{
		Connection connection = PoolBD.getConexion();

		String consultaSql = "INSERT INTO post(nickName, idReferencia, texto, urlFile) VALUES(?,?,?,?);";
		PreparedStatement stmt= connection.prepareStatement(consultaSql);
		stmt.setString(1, post.getNickName());
		stmt.setLong(2, post.getIdReferencia());
		stmt.setString(3, post.getTexto());
		stmt.setString(4, post.getUrlFile());

		ResultSet rs = stmt.executeQuery();
		connection.close();
	}
	public static void deletePost(long id) throws Exception{
		Connection connection = PoolBD.getConexion();

		String consultaSql = "DELETE from post WHERE id=?;";
		PreparedStatement stmt= connection.prepareStatement(consultaSql);
		stmt.setLong(1, id);

		ResultSet rs = stmt.executeQuery();
		connection.close();
	}
}	 
