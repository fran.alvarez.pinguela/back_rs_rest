package com.informaticapinguela.modelo;

public class Usuario {
	
	private String name;
	private String surName;
	private String nickName;
	private String email;
	private String password;
	private String role;
	private String image;
	
	public Usuario(String nickName, String Password){
		this.nickName = nickName;
		this.password = password;
	}
	
	public Usuario(String name, String surName, String nickName, String email, String password, String role, String image) {
		this.name = name;
		this.surName = surName;
		this.nickName = nickName;
		this.email = email;
		this.password = password;
		this.role = role;
		this.image = image;
	}
	
	public Usuario() {
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}
