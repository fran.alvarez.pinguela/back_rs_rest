package com.informaticapinguela.modelo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory;
import org.mariadb.jdbc.MariaDbDataSource;

public final class PoolBD{
	
	static MariaDbDataSource ds = new MariaDbDataSource();

	static {
		try {
			ds.setUrl("jdbc:mariadb://localhost:3306/redsocial");
			ds.setUser("root");
			ds.setPassword("abc123.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static Connection getConexion() throws SQLException {;
		return ds.getConnection();
	}

}
