package com.informaticapinguela.modelo;

public class Post {
	long id;
	String nickName;
	long idReferencia;
	String texto;
	String urlFile;
	
	public Post(long id, String nickName, long idReferencia, String texto, String urlFile) {
		super();
		this.id = id;
		this.nickName = nickName;
		this.idReferencia = idReferencia;
		this.texto = texto;
		this.urlFile = urlFile;
	}
	
	public Post(long id, String nickName, String texto) {
		super();
		this.id = id;
		this.nickName = nickName;
		this.texto = texto;
	}
	
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public long getIdReferencia() {
		return idReferencia;
	}
	public void setIdReferencia(long idReferencia) {
		this.idReferencia = idReferencia;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getUrlFile() {
		return urlFile;
	}
	public void setUrlFile(String urlFile) {
		this.urlFile = urlFile;
	}

}
