package com.informaticapinguela.servicios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;

import com.informaticapinguela.modelo.PoolBD;
import com.informaticapinguela.modelo.Usuario;

public class Autenticador {
	
	public static boolean autenticar(Usuario usuario) throws Exception{
		boolean status;
			Connection connection = PoolBD.getConexion();
			
			String consultaSql = "select * from user where nickName=? and password=?";
			PreparedStatement stmt= connection.prepareStatement(consultaSql);
			stmt.setString(1, usuario.getNickName());
			stmt.setString(2, usuario.getPassword());
			ResultSet rs = stmt.executeQuery(); 
			if (rs.next()){
				status= true;
			} else {
				status = false;
			}
			connection.close();
			return status;
	}
}
