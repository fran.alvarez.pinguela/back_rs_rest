package com.informaticapinguela.servicios;

import java.io.IOException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.informaticapinguela.modelo.UsuarioDAO;

@Provider
@PreMatching
public class Interceptor implements ContainerRequestFilter{
	
	@Context
	private HttpServletRequest servletRequest;
	public Interceptor( ) {
		
	}

	@Override
	public void filter(ContainerRequestContext request) throws IOException {
		String url = request.getUriInfo().getAbsolutePath().toString();
		if (url.contains("/api/auth")) {
			return;
			}else if(url.contains("/api/register")) {
				return;}
		
		String token= request.getHeaderString("Authorization");
		
		
		if (token==null) {
			JsonObject json = Json.createObjectBuilder().add("mensaje", "credenciales son necesarias").build();
			request.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(json).type(MediaType.APPLICATION_JSON).build());
			return;
		}
		
		/*String ipNick = servletRequest.getRemoteAddr()+servletRequest.getHeader("nickName");
		if (!Cache.validar(ipNick, token)) {
			System.out.println("Usuario invalido");
			UsuarioDAO.getUsuarios();
			JsonObject json = Json.createObjectBuilder().add("mensaje", "credenciales incorrectas").build();
			request.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(json).type(MediaType.APPLICATION_JSON).build());
			return;
		}*/
		
	}

}
