package com.informaticapinguela.servicios;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

public class Cache {
	private static Hashtable ipNick_token = new Hashtable();
	
	public static void addToken(String ipNick, String token) {
		Iterator<Map.Entry<Integer, String>> itr = ipNick_token.entrySet().iterator();
		 
		Map.Entry<Integer, String> entry = null;
		while(itr.hasNext()){
		    entry = itr.next();
		    System.out.println( entry.getKey() + "->" + entry.getValue() );
		}
		ipNick_token.put(ipNick, token);
	}
	
	public static String getToken(String ip) {
		return (String) ipNick_token.get(ip);
	}
	
	public static boolean validar(String ipNick, String token) {
		System.out.println("ipNick: "+ipNick);
		System.out.println("token: "+token);
		if((ipNick_token.get(ipNick).equals(token)) && token!=null) {
			return true;
		}
		return false;
	}
	
	public static String generateNewToken() {
		SecureRandom secureRandom = new SecureRandom();
		Base64.Encoder base64Encoder = Base64.getUrlEncoder();
		
	    byte[] randomBytes = new byte[24];
	    secureRandom.nextBytes(randomBytes);//Generamos 24 bytes aleatorios
	    return base64Encoder.encodeToString(randomBytes);
	}
	
}
