package com.informaticapinguela.servicios;

import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context; 
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.informaticapinguela.modelo.Post;
import com.informaticapinguela.modelo.PostDAO;
import com.informaticapinguela.modelo.Usuario;
import com.informaticapinguela.modelo.UsuarioDAO;

@ApplicationPath("api")
public class ControladorHttp extends Application{

	@Path("auth")
	public static class autenticador{
		@POST 
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public Response validar(Usuario usuario, @Context HttpServletRequest request) {
			System.out.println("Usuario validado");
			boolean status;
			Usuario usuarioDevuelto = null;
			try {
				status = Autenticador.autenticar(usuario);
				usuarioDevuelto = UsuarioDAO.getUsuario(usuario.getNickName());
				
			} catch (Exception e) {
				status = false;
				System.out.println("Error SQL");
			}
			
			if (status && usuarioDevuelto != null) {
				String Hash = Cache.generateNewToken();
				String ipNick = request.getRemoteAddr()+usuario.getNickName();
				Cache.addToken(ipNick, Hash);
				JsonObject json = Json.createObjectBuilder().add("token", Hash).add("nickName",usuarioDevuelto.getNickName()).add("name",usuarioDevuelto.getName()).add("email",usuarioDevuelto.getEmail()).add("role",usuarioDevuelto.getRole()).add("image",usuarioDevuelto.getImage()).add("surName",usuarioDevuelto.getSurName()).add("password",usuarioDevuelto.getPassword()).build();
				//devolvemos el string serializado en json, de esta forma lo podemos deserializar en un String en el front
				return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin","*").entity(json).build();
			}
			JsonObject json = Json.createObjectBuilder().add("mensaje", "Datos incorrectos").build();
			return Response.status(Response.Status.UNAUTHORIZED).entity(json).build();	
		}
		
	}
	
	
	@Path("register")
	public static class Registro{
		@POST
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public static Response registerUser(Usuario usuario) {
			
			System.out.println(usuario.getNickName());
			try {
				if(UsuarioDAO.usuarioValidoRegistro(usuario)) {
					UsuarioDAO.putUsuario(usuario);
					return Response.ok(usuario).build();
				}else {
					return Response.serverError().build();
				}
				
			} catch (Exception e) {
				System.out.println(e);
			}
			
			return Response.serverError().build();
		}
	}
	@Path("posts/{nickName}")
	public static class Posts{
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public static Response getPosts(@Context HttpServletRequest servletRequest, @PathParam("nickName") String nickName) {
			ArrayList<Post> postsUsuario;
			try {
				System.out.println("NickName recibido: "+nickName);
				postsUsuario = PostDAO.getPost(nickName);
				return Response.ok(postsUsuario).build();
			} catch (Exception e) {
				System.out.println("Error");
				return Response.serverError().build();
			}

		}
	}
	
	@Path("counters")
	public static class Contador{
		@Path("{userNickName}")
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public static Response getPosts(@Context HttpServletRequest servletRequest, @PathParam("userNickName") String userNickName) {
			try {
				System.out.println("NickName recibido: "+userNickName);
				Integer numberOfFollowers = UsuarioDAO.getNumberOfFollowers(userNickName);
				Integer numberOfFollowing = UsuarioDAO.getNumberOfFollowing(userNickName);
				Integer numberOfPosts = UsuarioDAO.getNumberOfPosts(userNickName);
				
				JsonObject json = Json.createObjectBuilder().add("followers", numberOfFollowers.toString()).add("following",numberOfFollowing.toString()).add("posts",numberOfPosts.toString()).build();
				return Response.status(Response.Status.CREATED).header("Access-Control-Allow-Origin","*").entity(json).build();
			} catch (Exception e) {
				System.out.println("Error");
				return Response.serverError().build();
			}

		}
		/*
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public static Response getPosts(@Context HttpServletRequest servletRequest) {
			ArrayList<Post> postsUsuario;
			try {
				System.out.println("NickName recibido: "+nickName);
				postsUsuario = PostDAO.getPost(nickName);
				//JsonObject json = Json.createObjectBuilder().add("mensaje", "Datos incorrectos").build();
				return Response.ok(postsUsuario).build();
			} catch (Exception e) {
				System.out.println("Error");
				return Response.serverError().build();
			}

		}*/
	}
	
	@Path("user/{nickName}")
	public static class User{
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public static Response getUser(@Context HttpServletRequest servletRequest, @PathParam("nickName") String nickName) {
			String ipNick=servletRequest.getRemoteAddr() + nickName;
			if(Cache.validar(ipNick, servletRequest.getHeader("Authorization"))) {
				try {
					Usuario usuarioDevuelto = UsuarioDAO.getUsuario(nickName);
					return Response.ok(usuarioDevuelto).build();
				} catch (Exception e) {
				}
			}
			return Response.serverError().build();
		}
		/*Actualizar usuario*/
		@PUT
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public static Response putUser(@Context HttpServletRequest servletRequest, @PathParam("nickName") String nickName, Usuario usuario) {
			try {
				UsuarioDAO.updateUsuario(usuario);	
				return Response.ok().build();
			}catch(Exception e) {
				return Response.serverError().build();
			}
		}
		
		@DELETE
		  public static Response deleteUser(@PathParam("nickName") String nickName, @Context HttpServletRequest servletRequest) {
			String ipNick=servletRequest.getRemoteAddr() + nickName;
			if(Cache.validar(ipNick, servletRequest.getHeader("Authorization"))) {
				try {
					UsuarioDAO.deleteUsuario(nickName);
					return Response.ok("Usuario borrado").build();
				} catch (Exception e) {
					return Response.serverError().build();
				}
			}else {
				return Response.serverError().build();
			}
		  }
	}
}
